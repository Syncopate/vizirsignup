package dk.vizir.server;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import dk.vizir.client.vizirService;

public class vizirServiceImpl extends RemoteServiceServlet implements vizirService {
    // Implementation of sample interface method
    public String getMessage(String msg) {
        return "Client said: \"" + msg + "\"<br>Server answered: \"Hi!\"";
    }
}