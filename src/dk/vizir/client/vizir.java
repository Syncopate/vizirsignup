package dk.vizir.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.*;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.DOM;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.ui.Label;
import org.gwtbootstrap3.client.ui.*;
import org.gwtbootstrap3.client.ui.Button;
import org.gwtbootstrap3.client.ui.ListBox;
import org.gwtbootstrap3.client.ui.TextBox;

/**
 * Entry point classes define <code>onModuleLoad()</code>
 */
public class vizir implements EntryPoint {

    /**
     * This is the entry point method.
     */
    public void onModuleLoad() {

        org.gwtbootstrap3.client.ui.TextBox fornavn = new TextBox();
        org.gwtbootstrap3.client.ui.TextBox efternavn = new TextBox();
        org.gwtbootstrap3.client.ui.TextBox email = new TextBox();
        ListBox brancher = new ListBox();
        ListBox postnummer = new ListBox();
        Button go = new Button("Tilmeld!");
        Button reset = new Button("Start forfra");
        fornavn.setAllowBlank(false);
        org.gwtbootstrap3.client.ui.Form formGroup = new Form();

        formGroup.add(fornavn); formGroup.add(efternavn); formGroup.add(email); formGroup.add(brancher); formGroup.add(postnummer);

        formGroup.add(go); formGroup.add(reset);

        RootPanel.get("signupform").add(formGroup);

    }

    private static class MyAsyncCallback implements AsyncCallback<String> {
        private Label label;

        public MyAsyncCallback(Label label) {
            this.label = label;
        }

        public void onSuccess(String result) {
            label.getElement().setInnerHTML(result);
        }

        public void onFailure(Throwable throwable) {
            label.setText("Failed to receive answer from server!");
        }
    }
}
