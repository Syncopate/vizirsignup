package dk.vizir.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

@RemoteServiceRelativePath("vizirService")
public interface vizirService extends RemoteService {
    // Sample interface method of remote interface
    String getMessage(String msg);

    /**
     * Utility/Convenience class.
     * Use vizirService.App.getInstance() to access static instance of vizirServiceAsync
     */
    public static class App {
        private static vizirServiceAsync ourInstance = GWT.create(vizirService.class);

        public static synchronized vizirServiceAsync getInstance() {
            return ourInstance;
        }
    }
}
