package dk.vizir.client;

import com.google.gwt.user.client.rpc.AsyncCallback;

public interface vizirServiceAsync {
    void getMessage(String msg, AsyncCallback<String> async);
}
